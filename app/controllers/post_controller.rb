class PostController < ApplicationController
		before_action :authenticate_user! , only: [:index, :question, :answer]

		def index
				@user = current_user
				@ask = Ask.new
		end

		def show
				@user = User.find(params[:id])
				render 'index'
		end

		def list
				@users = User.all
		end

		def question
				Ask.create(user_id: current_user.id, to_id: params[:to], content: params[:ask], ask_image: params[:image_file])
				redirect_to :back
		end

		def answer
				if Ask.find(params[:ask_id]).to_id == current_user.id
				Answer.create(user_id: current_user.id, ask_id: params[:ask_id], content: params[:answer])
				end
				redirect_to :root
		end
end
