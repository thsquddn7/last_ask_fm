class Ask < ActiveRecord::Base
		belongs_to :user
		has_many :answers
		mount_uploader :ask_image, AskPhotoUploader
end
