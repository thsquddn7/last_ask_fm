class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :ask_id
      t.integer :user_id
      t.text :content

      t.timestamps null: false
    end
  end
end
