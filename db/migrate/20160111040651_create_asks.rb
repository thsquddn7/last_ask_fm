class CreateAsks < ActiveRecord::Migration
  def change
    create_table :asks do |t|
      t.integer :user_id
      t.integer :to_id
      t.text :content

						t.string :ask_image

      t.timestamps null: false
    end
  end
end
